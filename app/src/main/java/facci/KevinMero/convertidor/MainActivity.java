package facci.KevinMero.convertidor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    Button temperatura=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        temperatura=(Button)findViewById(R.id.button2);
        temperatura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar=new Intent(getApplicationContext(),Temperatura.class);
                startActivity(cambiar);
            }
        });

        Log.i("Aplicacion Movil 1", "Mero Avila Kevin");
    }
}
//Log: (metodos) o alternativas para poder imprimir en consola.